import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TestecpUiLibModule} from 'testecp-ui-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, TestecpUiLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
